import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'classes/Dialogs.dart';
import 'classes/styles.dart';
import 'classes/Dialogs.dart' as Dialogs;
import 'classes/globals.dart' as globals;
import 'classes/PostArticles.dart';
import 'classes/PostTexts.dart';
import 'classes/PostGallery.dart';
import 'package:http/http.dart' as http;

import 'menu.dart';
import 'routes/registry.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
        title: 'Casa Museo Miss Trinie',
        theme: ThemeData(
          primarySwatch: Colors.brown,
        ),
        home: MyHomePage(title: 'Casa Museo Miss Trinie'),
        debugShowCheckedModeBanner: false,
        debugShowMaterialGrid: false);
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({this.title}) : super();

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool exist = false;
  bool isLoading = true;
  bool _isSending = false;
  bool resetPass = false;
  bool editing = false;
  Future<PostTexts> postTexts;
  Future<PostArticles> postArticles;
  Future<PostGallery> postGallery;
  Future<void> value;
  String imgDefUri =
      "http://file-s.download/isysauthdb/webTransfer/imagenes/nobody.jpg";

  final mailController = TextEditingController();
  final passController = TextEditingController();

  Future<PostTexts> _fetchDataTexts() async {
    final response = await http
        .get("https://ebostpay.herokuapp.com/listarTexto/TRINIE@eBost");
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return PostTexts.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<PostArticles> _fetchDataArticles() async {
    final response = await http
        .get("https://ebostpay.herokuapp.com/listarArticulo/TRINIE@eBost");
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return PostArticles.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<PostGallery> _fetchDataGallery() async {
    final response = await http.get(
        "https://ebostpay.herokuapp.com/listarGaleriaPorTipo/Galeria/TRINIE@eBost");
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return PostGallery.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _start();
    if (globals.isLoading) {
      postTexts = _fetchDataTexts();
      postArticles = _fetchDataArticles();
      postGallery = _fetchDataGallery();
    }
  }

  Future<void> _start() async {
    await _requestPermissions();
  }

  Future<void> _checkExist() async {
    final prefs = await SharedPreferences.getInstance();
    final lan = prefs.getString('lan') ?? "";
    final img = prefs.getString('img') ?? "";
    final correo = prefs.getString('correo') ?? "";
    if (img.isNotEmpty) {
      imgDefUri = img;
    }
    if (correo.isNotEmpty) {
      mailController.text = correo;
    }
    if (lan.isNotEmpty) {
      globals.lan = lan;
      print(lan);
      setState(() {
        exist = true;
      });
    } else {
      await _openDialog();
    }
  }

  Future<void> _requestPermissions() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([
      PermissionGroup.camera,
      PermissionGroup.storage,
      PermissionGroup.photos
    ]);
  }

  Future<void> _openDialog() async {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) => Dialogs.SiteDialogOverlay(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final emailField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      color: Colors.white70,
      child: TextField(
        onTap: () => setState(() => editing = true),
        onEditingComplete: () => setState(() => editing = false),
        onSubmitted: (str) => setState(() => editing = false),
        controller: mailController,
        textAlign: TextAlign.start,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Email",
            prefixIcon: Icon(Icons.person),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(15.0))),
      ),
    );
    final passwordField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      color: Colors.white70,
      child: TextField(
        onTap: () => setState(() => editing = true),
        onEditingComplete: () => setState(() => editing = false),
        onSubmitted: (str) => setState(() => editing = false),
        controller: passController,
        textAlign: TextAlign.start,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Password",
            prefixIcon: Icon(Icons.lock),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(15.0))),
      ),
    );
    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(20.0),
      color: Colors.blueAccent,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: _sendData,
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    final registerButton = OutlineButton(
      borderSide: BorderSide(width: 0, color: Colors.transparent),
      onPressed: () => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Registry(
                  title: widget.title,
                )),
      ),
      highlightedBorderColor: Colors.transparent,
      disabledBorderColor: Colors.transparent,
      child: Text(
        "Registrarse",
        style: TextStyle(fontSize: 15, color: Colors.blueAccent, shadows: [
          Shadow(
              // bottomLeft
              offset: Offset(-1.5, -1.5),
              color: Colors.white),
          Shadow(
              // bottomRight
              offset: Offset(1.5, -1.5),
              color: Colors.white),
          Shadow(
              // topRight
              offset: Offset(1.5, 1.5),
              color: Colors.white),
          Shadow(
              // topLeft
              offset: Offset(-1.5, 1.5),
              color: Colors.white),
        ]),
      ),
    );

    final resetButton = OutlineButton(
      borderSide: BorderSide(width: 0, color: Colors.transparent),
      onPressed: _resetPass,
      highlightedBorderColor: Colors.transparent,
      disabledBorderColor: Colors.transparent,
      child: Text(
        "¿Olvidaste tu contraseña? Restablezcala aqui!",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 13, color: Colors.blueAccent, shadows: [
          Shadow(
              // bottomLeft
              offset: Offset(-1.5, -1.5),
              color: Colors.white),
          Shadow(
              // bottomRight
              offset: Offset(1.5, -1.5),
              color: Colors.white),
          Shadow(
              // topRight
              offset: Offset(1.5, 1.5),
              color: Colors.white),
          Shadow(
              // topLeft
              offset: Offset(-1.5, 1.5),
              color: Colors.white),
        ]),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.title,
          textAlign: TextAlign.center,
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          new Center(
            child: new FutureBuilder<PostTexts>(
                future: postTexts,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    globals.texts = snapshot.data.texts;
                    return new FutureBuilder<PostArticles>(
                        future: postArticles,
                        builder: (context2, snapshot2) {
                          if (snapshot2.hasData) {
                            globals.articles = snapshot2.data.articles;
                            return new FutureBuilder<PostGallery>(
                              future: postGallery,
                              builder: (context3, snapshot3) {
                                if (snapshot3.hasData) {
                                  globals.gallery = snapshot3.data.gallery;
                                  if (globals.isLoading) {
                                    _checkExist();
                                  }
                                  globals.isLoading = false;

                                  return new Container(
                                    height:
                                        MediaQuery.of(context).size.height - 80,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/bg_min.jpg"),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    child: _isSending
                                        ? new Center(
                                            child: new Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                new Text(globals.words[1]
                                                    [globals.lan]),
                                                new CircularProgressIndicator()
                                              ],
                                            ),
                                          )
                                        : Padding(
                                            padding: const EdgeInsets.all(36.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  decoration: ShapeDecoration(
                                                    shape: OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(80),
                                                        borderSide: BorderSide(
                                                            width: 3,
                                                            color:
                                                                Colors.white)),
                                                    color: Colors.white,
                                                  ),
                                                  child: CircleAvatar(
                                                    backgroundColor:
                                                        Colors.white,
                                                    child: GestureDetector(),
                                                    radius: 80.0,
                                                    backgroundImage: FadeInImage
                                                            .assetNetwork(
                                                                fit: BoxFit
                                                                    .cover,
                                                                placeholder:
                                                                    'assets/images/bar_loading.gif',
                                                                image:
                                                                    imgDefUri)
                                                        .image,
                                                  ),
                                                ),
                                                SizedBox(height: 35.0),
                                                Flexible(
                                                  fit: FlexFit.loose,
                                                  child: emailField,
                                                ),
                                                Flexible(
                                                  fit: FlexFit.loose,
                                                  child: passwordField,
                                                ),
                                                SizedBox(height: 10.0),
                                                Flexible(
                                                  fit: FlexFit.loose,
                                                  child: loginButton,
                                                ),
                                                Flexible(
                                                  fit: FlexFit.loose,
                                                  child: registerButton,
                                                ),
                                                Flexible(
                                                  fit: FlexFit.loose,
                                                  child: !resetPass
                                                      ? Text("")
                                                      : resetButton,
                                                ),
                                              ],
                                            ),
                                          ),
                                  );
                                } else {
                                  return new Center(
                                    child: new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        new Text(globals.words[1][globals.lan]),
                                        new CircularProgressIndicator()
                                      ],
                                    ),
                                  );
                                }
                              },
                            );
                          } else {
                            return new Center(
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text(globals.words[1][globals.lan]),
                                  new CircularProgressIndicator()
                                ],
                              ),
                            );
                          }
                        });
                  } else {
                    return new Center(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Text(globals.words[1][globals.lan]),
                          new CircularProgressIndicator()
                        ],
                      ),
                    );
                  }
                }),
          )
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: globals.isLoading
          ? null
          : editing
              ? null
              : FloatingActionButton.extended(
                  backgroundColor: Colors.black45,
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Menu(
                              title: widget.title,
                            )),
                  ),
                  label: Text("Omitir"),
                  icon: Icon(Icons.forward),
                ),
    );
  }

  bool _checkFields() {
    String mail = mailController.text ?? "";
    String pass = passController.text ?? "";

    if (mail.isNotEmpty && pass.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  Future<http.Response> _sendData() async {
    String mail = mailController.text;
    String pass = passController.text;

    if (_checkFields()) {
      setState(() {
        _isSending = true;
      });

      var params = {
        "correo": "$mail",
        "clave": "$pass",
      };

      Map<String, String> headers = {"Content-type": "application/json"};

      http.Response post = await http.post(
        "https://isysauthdb.herokuapp.com/api/authdb/cuenta/verify",
        headers: headers,
        body: json.encode(params),
        //headers: paramDic
      );
      print("" + post.statusCode.toString() + " - " + post.reasonPhrase);
      if (post.statusCode == 200) {
        print("Success");
        var body = json.decode(post.body);
        setState(() {
          _isSending = false;
        });
        if (body['ok']) {
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('img', body['data']['archivo_imagen_perfil']);
          prefs.setString('correo', body['data']['correo']);
          showBottomMessage("Inicio Exitoso!");
          globals.isLogged = true;
          globals.user = body;

          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Menu(
                      title: widget.title,
                    )),
          );
        } else {
          showBottomMessage("Cuenta no registrada o inactiva");
          resetPass = true;
          passController.text = "";
        }
      } else {
        print("ERROR");
      }

      return post;
    }
    return null;
  }

  Future<http.Response> _resetPass() async {
    String mail = mailController.text;

    setState(() {
      _isSending = true;
    });

    var params = {"correo": "$mail"};

    Map<String, String> headers = {"Content-type": "application/json"};

    http.Response post = await http.put(
      "https://isysauthdb.herokuapp.com/api/authdb/cuenta/reset/clave",
      headers: headers,
      body: json.encode(params),
      //headers: paramDic
    );
    print("" + post.statusCode.toString() + " - " + post.reasonPhrase);
    if (post.statusCode == 200) {
      print("Success");
      var body = json.decode(post.body);
      setState(() {
        _isSending = false;
      });
      if (body['ok']) {
        showBottomMessage("Contraseña enviada a su correo Exitosamente!");
        setState(() {
          resetPass = false;
        });
      } else {
        showBottomMessage("Error de envio, revise el correo electronico");
        resetPass = true;
        passController.text = "";
      }
    } else {
      print("ERROR");
    }

    return post;
  }
}
