import 'dart:convert';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'styles.dart';

//Toast's
void showBottomMessage(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      fontSize: 12,
      backgroundColor: Colors.black87,
      textColor: Colors.white,
      gravity: ToastGravity.BOTTOM,
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIos: 2);
}

//Lan Dialog

class SiteDialogOverlay extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SiteDialogOverlayState();
}

class SiteDialogOverlayState extends State<SiteDialogOverlay>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  bool cond = true;
  String _radioValue; //Initial definition of radio button value
  String choice = globals.lan;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    _radioValue = choice;
  }

  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      switch (value) {
        case 'es':
          choice = value;
          break;
        case 'en':
          choice = value;
          break;
        case 'fr':
          choice = value;
          break;
        case 'pt':
          choice = value;
          break;
        case 'de':
          choice = value;
          break;
        default:
          choice = 'es';
      }
      debugPrint(choice); //Debug the choice in console
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(20.0)),
        child: new ListView(
          padding: EdgeInsets.all(16),
          shrinkWrap: true,
          children: <Widget>[
            new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(
                  "" + globals.texts[13][globals.lan],
                  style: titleFont,
                ),
                new Padding(padding: EdgeInsets.only(bottom: 20)),
                new Container(
                  height: 50,
                  child: new ListView(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new CircleAvatar(
                            child: GestureDetector(
                              onTap: () async {
                                final prefs =
                                    await SharedPreferences.getInstance();
                                final lan = prefs.setString("lan", "es");
                                setState(() {
                                  globals.lan = "es";
                                  print("$lan");
                                });
                                Navigator.of(context).pop();
                              },
                            ),
                            radius: 25.0,
                            backgroundImage:
                                AssetImage("assets/images/spain.png"),
                          ),
                          new Padding(padding: EdgeInsets.only(left: 5)),
                          new CircleAvatar(
                            child: GestureDetector(
                              onTap: () async {
                                final prefs =
                                    await SharedPreferences.getInstance();
                                final lan = prefs.setString("lan", "en");
                                setState(() {
                                  globals.lan = "en";
                                  print("$lan");
                                });
                                Navigator.of(context).pop();
                              },
                            ),
                            radius: 25.0,
                            backgroundImage:
                                AssetImage("assets/images/united_states.png"),
                          ),
                          new Padding(padding: EdgeInsets.only(left: 5)),
                          new CircleAvatar(
                            child: GestureDetector(
                              onTap: () async {
                                final prefs =
                                    await SharedPreferences.getInstance();
                                final lan = prefs.setString("lan", "pt");
                                setState(() {
                                  globals.lan = "pt";
                                  print("$lan");
                                });
                                Navigator.of(context).pop();
                              },
                            ),
                            radius: 25.0,
                            backgroundImage:
                                AssetImage("assets/images/portugal.png"),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
            ),
          ],
        ));
  }
}

class SendingImageDialog extends StatefulWidget {
  final String mail;
  final String img;

  const SendingImageDialog({Key key, this.mail, this.img}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SendingImageDialogState(mail, img);
}

class _SendingImageDialogState extends State<SendingImageDialog> {
  final String mail;
  final String img;
  ProgressDialog pr;

  _SendingImageDialogState(this.mail, this.img);

  Future<http.Response> callWebService() async {
    pr.show();
    final Map<String, dynamic> params = {"img": img, "correo": mail};

    Map<String, String> headers = {"Content-type": "application/json"};

    final post = await http.put(
        "https://isysauthdb.herokuapp.com/api/authdb/cuenta/edit/perfil",
        body: json.encode(params),
        headers: headers);

    print(post.statusCode.toString() + " - " + post.reasonPhrase);
    print(json.decode(post.body)['message']);
    if (post.statusCode == 200) {
      showBottomMessage("Imagen cambiada correctamente!");
      pr.hide();
      setState(() {
        globals.user['data']['archivo_imagen_perfil'] = img;
      });
      Navigator.pop(context);
      Navigator.pop(context);
    } else {
      pr.hide();
      showBottomMessage("Error al cambiar la imagen");
      Navigator.pop(context);
    }
    return post;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);
    pr.style(
        progressWidget: Center(
          child: CircularProgressIndicator(),
        ),
        borderRadius: 20.0,
        message: 'Subiendo Imagen');
    callWebService();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
      child: new Container(
        child: new SizedBox(
          child: new Center(
            child: new CircularProgressIndicator(),
          ),
          height: 100,
          width: 60,
        ),
      ),
    );
  }
}
