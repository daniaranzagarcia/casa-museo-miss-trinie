class PostElements {
  final List elements;

  PostElements({this.elements});

  factory PostElements.fromJson(Map<String, dynamic> json) {
    return new PostElements(
      elements: json['data'] as List,
    );
  }
}
