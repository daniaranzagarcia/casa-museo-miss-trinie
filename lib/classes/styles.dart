import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);


//Title Style
TextStyle titleFont =
    new TextStyle(fontWeight: FontWeight.bold, fontSize: 22, shadows: [
  Shadow(
      // bottomLeft
      offset: Offset(-1.5, -1.5),
      color: Colors.white),
  Shadow(
      // bottomRight
      offset: Offset(1.5, -1.5),
      color: Colors.white),
  Shadow(
      // topRight
      offset: Offset(1.5, 1.5),
      color: Colors.white),
  Shadow(
      // topLeft
      offset: Offset(-1.5, 1.5),
      color: Colors.white),
]);

//Subtitle
TextStyle subtitleFont =
    new TextStyle(fontWeight: FontWeight.bold, fontSize: 16);
TextStyle subtitleFontWh = new TextStyle(
    fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white);

//Standard
TextStyle standardFont =
    new TextStyle(fontWeight: FontWeight.normal, fontSize: 14);
TextStyle standardFontWh = new TextStyle(
    fontWeight: FontWeight.normal, fontSize: 14, color: Colors.white);

//Mini Standard
TextStyle ministandardFont =
    new TextStyle(fontWeight: FontWeight.normal, fontSize: 12);
TextStyle ministandardFontWh = new TextStyle(
    fontWeight: FontWeight.normal, fontSize: 12, color: Colors.white);
