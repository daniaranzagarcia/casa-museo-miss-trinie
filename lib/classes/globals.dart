library trinie.globals;

import 'dart:convert';

import 'package:http/http.dart' as http;

import 'Dialogs.dart';

String lan = "es";
List texts = new List();
List articles = new List();
List gallery = new List(); //
bool isLoading = true;
bool isLogged = false;
var words = [
  {
    'es': 'Menú de Inicio',
    'en': 'Main Menu',
    'fr': 'Menu Démarrer',
    'pt': 'Menu Iniciar',
    'de': 'Startmenü',
  },
  {
    'es': 'Cargando Datos',
    'en': 'Loading Data',
    'fr': 'Chargement des données',
    'pt': 'Carregando dados',
    'de': 'Wird geladen',
  },
  {
    'es': 'Seleccionar',
    'en': 'Select',
    'fr': 'Sélectionner',
    'pt': 'Selecione',
    'de': 'Wählen',
  },
  {
    'es': 'Biografía',
    'en': 'Biography',
    'fr': 'Biographie',
    'pt': 'Biografia',
    'de': 'Biografie',
  },
  {
    'es': 'Galería',
    'en': 'Gallery',
    'fr': 'Galerie',
    'pt': 'Galeria',
    'de': 'Galerie',
  },
  {
    'es': 'Reseña',
    'en': 'Review',
    'fr': 'Revue',
    'pt': 'Revisão',
    'de': 'Rückblick',
  },
  {
    'es': 'Antigüedades',
    'en': 'Antiques',
    'fr': 'Antiquités',
    'pt': 'Antiguidades',
    'de': 'Antiquitäten',
  }
];
var minLv = 300;
var user;

int getCurrentLvPt(int lv) {
  return (minLv + (lv * 100) + (100 * (lv / 100))).round();
}

int getCurrentPerc(int CurrentLv, int points) {
  return ((points / CurrentLv) * 100).round();
}

int getDiscount(int minDis, int lv) {
  return (minDis - (minDis * ((lv * 2) / 100))).round();
}

void sumPt(int pt) {
  user['data']['puntos'] += pt;
}

Future<http.Response> sendPt(int pt) async {
  var params = {"correo": user['data']['correo'], "puntos": pt};

  Map<String, String> headers = {"Content-type": "application/json"};

  http.Response post = await http.put(
    "https://isysauthdb.herokuapp.com/api/authdb/cuenta/add/puntos",
    headers: headers,
    body: json.encode(params),
    //headers: paramDic
  );
  print("" + post.statusCode.toString() + " - " + post.reasonPhrase);
  if (post.statusCode == 200) {
    print("Success");
    var body = json.decode(post.body);

    if (body['ok']) {
      sumPt(pt);
      showBottomMessage("Se Han Sumado $pt Puntos!");
      await upLv();
    } else {
      showBottomMessage("Error al Conseguir Puntos");
    }
  } else {
    print("ERROR");
  }

  return post;
}

Future<http.Response> upLv() async {
  if (user['data']['puntos'] >= getCurrentLvPt(user['data']['nivel'])) {
    var params = {
      "correo": user['data']['correo'],
      "nivel": user['data']['nivel'] + 1
    };

    Map<String, String> headers = {"Content-type": "application/json"};

    http.Response post = await http.put(
      "https://isysauthdb.herokuapp.com/api/authdb/cuenta/edit/nivel",
      headers: headers,
      body: json.encode(params),
      //headers: paramDic
    );
    print("" + post.statusCode.toString() + " - " + post.reasonPhrase);
    if (post.statusCode == 200) {
      print("Success");
      var body = json.decode(post.body);

      if (body['ok']) {
        user['data']['nivel']++;
        showBottomMessage("Se Han Subido de Nivel!");
      } else {
        showBottomMessage("Error al Subir de Nivel");
      }
    } else {
      print("ERROR");
    }

    return post;
  } else {
    return null;
  }
}
