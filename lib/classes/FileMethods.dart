import 'dart:async';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();
  return directory.path;
}

Future<File> get localFile async {
  final path = await _localPath;
  return File('$path/lan.txt');
}

Future<File> writeContent(String data) async {
  final file = await localFile;
  // Write the file
  return file.writeAsString(data);
}

Future<String> readcontent() async {
  try {
    final file = await localFile;
    // Read the file
    String contents = await file.readAsString();
    return contents;
  } catch (e) {
    print(e.toString());
    return 'Error:';
  }
}
