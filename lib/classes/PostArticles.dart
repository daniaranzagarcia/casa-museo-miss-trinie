class PostArticles {
  final List articles;

  PostArticles({this.articles});

  factory PostArticles.fromJson(Map<String, dynamic> json) {
    return new PostArticles(
      articles: json['data'] as List,
    );
  }
}
