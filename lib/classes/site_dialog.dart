import 'package:flutter/material.dart';
import 'dart:io';

import 'package:video_player/video_player.dart';
import 'package:casa_museo_miss_trinie/classes/globals.dart' as globals;

class VideoPlayerScreen extends StatefulWidget {
  VideoPlayerScreen({Key key, this.url}) : super(key: key);

  final String url;

  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState(url: url);
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  _VideoPlayerScreenState({Key key, this.url}) : super();

  final String url;
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    // Using an mp3 file instead of mp4.
    _controller = VideoPlayerController.network(
      url,
    );

    _initializeVideoPlayerFuture = _controller.initialize();

    _controller.setLooping(true);
    SumPt(20);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Dialog(
      child: Container(
        padding: EdgeInsets.all(1),
        height: 100,
        child: Center(
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              FutureBuilder(
                future: _initializeVideoPlayerFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    print('done');
                    // not wrapped in an AspectRatio widget
                    return Container(
                      child: VideoPlayer(_controller),
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
              new FlatButton.icon(
                  padding: EdgeInsets.all(5),
                  onPressed: () {
                    setState(() {
                      if (_controller.value.isPlaying) {
                        _controller.pause();
                      } else {
                        _controller.play();
                      }
                    });
                  },
                  icon: Icon(
                    _controller.value.isPlaying
                        ? Icons.pause
                        : Icons.play_arrow,
                    color: Colors.white,
                  ),
                  label: Text(""))
            ],
          ),
        ),
      ),
    );
  }

  void SumPt(int pt) async {
    await globals.sendPt(pt);
  }
}
