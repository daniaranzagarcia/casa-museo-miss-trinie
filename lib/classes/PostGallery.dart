class PostGallery {
  final List gallery;

  PostGallery({this.gallery});

  factory PostGallery.fromJson(Map<String, dynamic> json) {
    return new PostGallery(
      gallery: json['data'] as List,
    );
  }
}
