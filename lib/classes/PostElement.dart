class PostElement {
  var element;

  PostElement({this.element});

  factory PostElement.fromJson(Map<String, dynamic> json) {
    return new PostElement(
      element: json['data'],
    );
  }
}
