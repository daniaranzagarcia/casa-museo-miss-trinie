class PostSubCategories {
  final List subCat;

  PostSubCategories({this.subCat});

  factory PostSubCategories.fromJson(Map<String, dynamic> json) {
    return new PostSubCategories(
      subCat: json['data'][0]['subcategorias'] as List,
    );
  }
}
