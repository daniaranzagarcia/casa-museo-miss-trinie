class PostTexts {
  final List texts;

  PostTexts({this.texts});

  factory PostTexts.fromJson(Map<String, dynamic> json) {
    return new PostTexts(
      texts: json['data'] as List,
    );
  }
}
