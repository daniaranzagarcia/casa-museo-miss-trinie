import 'dart:async';

import 'package:casa_museo_miss_trinie/main.dart';
import 'package:casa_museo_miss_trinie/routes/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'classes/styles.dart';
import 'routes/biography.dart';
import 'routes/faq.dart';
import 'routes/museum.dart';
import 'classes/Dialogs.dart' as Dialogs;
import 'classes/globals.dart' as globals;
import 'classes/scan.dart' as sc;

class Menu extends StatefulWidget {
  Menu({this.title}) : super();

  final String title;

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Future<void> _openDialog() async {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) => Dialogs.SiteDialogOverlay(),
    );
  }

  //Menu
  void _MenuActions(String value) {
    switch (int.parse(value)) {
      case 0:
        {
          _openDialog();
          break;
        }
      case 1:
        {
          sc.getScan(context);
          break;
        }
      case 2:
        {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => User(
                      title: widget.title,
                    )),
          );
          break;
        }
      case 3:
        {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Faq(
                      title: widget.title,
                    )),
          );
          break;
        }
      case 4:
        {
          logout();
          setState(() {
            globals.isLogged = false;
            globals.isLoading = true;
          });
          Navigator.pop(context);
          Navigator.push(context, MaterialPageRoute(builder: (_) => MyApp()));
          break;
        }
    }
  }

  void logout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("img", "");
    prefs.setString("correo", "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            PopupMenuButton(
              onSelected: _MenuActions,
              itemBuilder: (_) => <PopupMenuItem<String>>[
                new PopupMenuItem(
                  child: FlatButton.icon(
                    icon: Icon(
                      Icons.language,
                      color: Colors.black54,
                    ),
                    label: Text(
                      globals.texts[13][globals.lan],
                      style: TextStyle(color: Colors.black54),
                    ),
                  ),
                  value: '0',
                ),
                new PopupMenuItem(
                  child: FlatButton.icon(
                    icon: Icon(
                      Icons.camera_alt,
                      color: Colors.black54,
                    ),
                    label: Text(
                      globals.texts[21][globals.lan],
                      style: TextStyle(color: Colors.black54),
                    ),
                  ),
                  value: '1',
                ),
                globals.isLogged
                    ? new PopupMenuItem(
                        child: FlatButton.icon(
                          icon: Icon(
                            Icons.person,
                            color: Colors.black54,
                          ),
                          label: Text(
                            "Cuenta",
                            style: TextStyle(color: Colors.black54),
                          ),
                        ),
                        value: '2',
                      )
                    : null,
                new PopupMenuItem(
                  child: FlatButton.icon(
                    icon: Icon(
                      Icons.info,
                      color: Colors.black54,
                    ),
                    label: Text(
                      globals.texts[22][globals.lan],
                      style: TextStyle(color: Colors.black54),
                    ),
                  ),
                  value: '3',
                ),
                globals.isLogged
                    ? new PopupMenuItem(
                        child: FlatButton.icon(
                          icon: Icon(
                            Icons.exit_to_app,
                            color: Colors.black54,
                          ),
                          label: Text(
                            "Cerrar Sesion",
                            style: TextStyle(color: Colors.black54),
                          ),
                        ),
                        value: '4',
                      )
                    : null
              ],
            )
          ],
        ),
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_min.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  new Padding(padding: EdgeInsets.all(16)),
                  Text(
                    "" + globals.words[0][globals.lan],
                    textAlign: TextAlign.center,
                    style: titleFont,
                  ),
                  new Padding(padding: EdgeInsets.only(bottom: 50)),
                  new Flexible(
                    fit: FlexFit.loose,
                    child: CircleAvatar(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Biography(
                                      title: widget.title,
                                    )),
                          );
                        },
                      ),
                      radius: 100.0,
                      backgroundImage: AssetImage("assets/images/foto1.jpg"),
                    ),
                  ),
                  new Padding(padding: EdgeInsets.only(bottom: 30)),
                  new Flexible(
                    fit: FlexFit.loose,
                    child: CircleAvatar(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Museum(
                                      title: widget.title,
                                    )),
                          );
                        },
                      ),
                      radius: 100.0,
                      backgroundImage: AssetImage("assets/images/foto2.jpg"),
                    ),
                  ),
                ],
              ),
            )));
  }
}
