import 'dart:convert';
import 'dart:io';

import 'package:casa_museo_miss_trinie/classes/Dialogs.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:casa_museo_miss_trinie/classes/styles.dart';
import 'package:flutter_rounded_progress_bar/flutter_icon_rounded_progress_bar.dart';
import 'package:flutter_rounded_progress_bar/flutter_rounded_progress_bar.dart';
import 'package:flutter_rounded_progress_bar/rounded_progress_bar_style.dart';
import 'package:http/http.dart' as http;
import 'package:casa_museo_miss_trinie/classes/globals.dart' as globals;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class User extends StatefulWidget {
  User({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _UserState createState() => _UserState();
}

class _UserState extends State<User> {
  String imgDefUri =
      "http://file-s.download/isysauthdb/webTransfer/imagenes/nobody.jpg";

  final passController = TextEditingController();
  final npassController = TextEditingController();
  final mailController = TextEditingController();
  final ptController = TextEditingController();
  bool _isSending = false;
  ProgressDialog pr;
  String imagen;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.imagen = globals.user['data']['archivo_imagen_perfil'];
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);
    pr.style(
        progressWidget: Center(
          child: CircularProgressIndicator(),
        ),
        borderRadius: 20.0,
        message: 'Subiendo Imagen');
  }

  //

  String b64Img = "";
  void _getImage() async {
    pr.show();
    try {
      showBottomMessage("Escoja la imagen, se recomienda una imagen cuadrada");
      File file = await FilePicker.getFile(type: FileType.IMAGE);
      List<int> imageBytes = await file.readAsBytes();
      String base64Image = base64Encode(imageBytes);
      b64Img = base64Image;
      await callWebService();
    } catch (e) {
      String name = globals.user["data"]['nombre_asociado'];
      _sendImg(
          "https://ui-avatars.com/api/?name=$name&background=4c75a3&color=ECEFF1&size=256",
          globals.user['data']['correo']);
    }
  }

  Future<http.Response> callWebService() async {
    final Map<String, dynamic> params = {
      "email_perfil": "" + globals.user['data']['correo'],
      "foto_perfil": b64Img
    };

    Map<String, String> headers = {"Content-type": "application/json"};

    print(json.encode(params));
    final post = await http.post(
        "https://file-s.download/isysauthdb/webTransfer/api/webService.php",
        body: json.encode(params));

    print(post.statusCode.toString() + " - " + post.reasonPhrase);

    if (post.statusCode == 200) {
      print("Success - Imagen Generada");
      var imgUri = Uri.encodeFull((json.decode(post.body))['data']).toString();
      print(imgUri);
      _sendImg(imgUri, globals.user['data']['correo']);
    }

    return post;
  }

  Future<http.Response> _sendImg(String img, String mail) async {
    final Map<String, dynamic> params = {"img": img, "correo": mail};

    Map<String, String> headers = {"Content-type": "application/json"};

    final post = await http.put(
        "https://isysauthdb.herokuapp.com/api/authdb/cuenta/edit/perfil",
        body: json.encode(params),
        headers: headers);

    print(post.statusCode.toString() + " - " + post.reasonPhrase);
    print(json.decode(post.body)['message']);
    if (post.statusCode == 200) {
      showBottomMessage("Imagen cambiada correctamente!");
      final prefs = await SharedPreferences.getInstance();
      prefs.setString("img", img);
      pr.hide();
      setState(() {
        globals.user['data']['archivo_imagen_perfil'] = img;
      });
      setState(() {
        this.imagen = img;
      });
      Navigator.pop(context);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => User(
                    title: widget.title,
                  )));
    } else {
      pr.hide();
      showBottomMessage("Error al cambiar la imagen");
      Navigator.pop(context);
    }
    return post;
  }

  //

  @override
  Widget build(BuildContext context) {
    final progressBar = RoundedProgressBar(
      milliseconds: 1000,
      height: 30,
      childCenter: Text(
        globals.user['data']['puntos'].toString() +
            "Pt - " +
            globals
                .getCurrentPerc(
                    globals.getCurrentLvPt(globals.user['data']['nivel']),
                    globals.user['data']['puntos'])
                .toString() +
            "%",
        textAlign: TextAlign.center,
        style: style.copyWith(
            color: Colors.black38, fontWeight: FontWeight.bold, fontSize: 16),
      ),
      style: RoundedProgressBarStyle(
          colorBackgroundIcon: Colors.blueAccent,
          colorProgress: Colors.blueAccent,
          colorProgressDark: Colors.transparent,
          colorBorder: Colors.white70,
          backgroundProgress: Colors.transparent,
          borderWidth: 3,
          widthShadow: 3),
      margin: EdgeInsets.symmetric(vertical: 16),
      borderRadius: BorderRadius.circular(32),
      percent: globals
          .getCurrentPerc(globals.getCurrentLvPt(globals.user['data']['nivel']),
              globals.user['data']['puntos'])
          .toDouble(),
    );

    final nameField = Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
        color: Colors.white70,
        child: Padding(
            padding: EdgeInsets.all(10),
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(
                globals.user['data']['nombre_asociado'],
                style: style,
                textAlign: TextAlign.center,
              ),
            )));

    final emailField = Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
        color: Colors.white70,
        child: Padding(
            padding: EdgeInsets.all(10),
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(
                globals.user['data']['correo'],
                style: style,
                textAlign: TextAlign.center,
              ),
            )));

    final resetButton = OutlineButton(
      borderSide: BorderSide(width: 0, color: Colors.transparent),
      onPressed: _OpenChangePassDialog,
      highlightedBorderColor: Colors.transparent,
      disabledBorderColor: Colors.transparent,
      child: Text(
        "Cambiar Contraseña",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 16, color: Colors.blueAccent, shadows: [
          Shadow(
              // bottomLeft
              offset: Offset(-1.5, -1.5),
              color: Colors.white),
          Shadow(
              // bottomRight
              offset: Offset(1.5, -1.5),
              color: Colors.white),
          Shadow(
              // topRight
              offset: Offset(1.5, 1.5),
              color: Colors.white),
          Shadow(
              // topLeft
              offset: Offset(-1.5, 1.5),
              color: Colors.white),
        ]),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.title,
          textAlign: TextAlign.center,
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          new Center(
              child: Container(
            height: MediaQuery.of(context).size.height - 80,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_min.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                      ),
                      Center(
                        child: Container(
                          decoration: ShapeDecoration(
                            shape: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(80),
                                borderSide:
                                    BorderSide(width: 3, color: Colors.white)),
                            color: Colors.white,
                          ),
                          child: CircleAvatar(
                            backgroundColor: Colors.white,
                            radius: 80.0,
                            backgroundImage: FadeInImage.assetNetwork(
                                    fit: BoxFit.cover,
                                    placeholder:
                                        'assets/images/bar_loading.gif',
                                    image: imagen)
                                .image,
                          ),
                        ),
                      ),
                      Positioned(
                          bottom: 0,
                          right: 80,
                          child: Card(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32)),
                            color: Colors.white,
                            child: SizedBox(
                              width: 40,
                              height: 40,
                              child: IconButton(
                                icon: Icon(
                                  Icons.edit,
                                  color: Colors.black26,
                                  semanticLabel: "Edit",
                                ),
                                iconSize: 25,
                                onPressed: _getImage,
                              ),
                            ),
                          )),
                      Positioned(
                          bottom: 30,
                          left: 60,
                          child: Card(
                            elevation: 5,
                            shape: CircleBorder(),
                            color: Colors.white,
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                "Lv" + globals.user['data']['nivel'].toString(),
                                style: style.copyWith(
                                    color: Colors.black38,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )),
                      Positioned(
                          bottom: 0,
                          left: 90,
                          child: Card(
                            elevation: 5,
                            shape: CircleBorder(),
                            color: Colors.white,
                            child: SizedBox(
                              width: 32,
                              height: 32,
                              child: IconButton(
                                icon: Icon(
                                  Icons.send,
                                  color: Colors.black26,
                                  semanticLabel: "Transfer",
                                ),
                                iconSize: 17,
                                onPressed: _OpenPassPtDialog,
                              ),
                            ),
                          ))
                    ],
                  ),
                  progressBar,
                  SizedBox(height: 45.0),
                  Flexible(
                    child: nameField,
                    fit: FlexFit.loose,
                  ),
                  SizedBox(height: 5.0),
                  Flexible(
                    child: emailField,
                    fit: FlexFit.loose,
                  ),
                  SizedBox(height: 5.0),
                  Flexible(
                    child: resetButton,
                    fit: FlexFit.loose,
                  ),
                ],
              ),
            ),
          ))
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.redAccent,
          icon: Icon(Icons.delete),
          onPressed: _delAccount,
          label: Text(
            "Eliminar Cuenta",
            style: style,
          )),
    );
  }

  Future<http.Response> _delAccount() async {
    pr.style(
        progressWidget: Center(
          child: CircularProgressIndicator(),
        ),
        borderRadius: 20.0,
        message: 'Cargando Solicitud');
    pr.show();
    final Map<String, dynamic> params = {
      "estado": "Inactivo",
      "correo": globals.user['data']['correo']
    };

    Map<String, String> headers = {"Content-type": "application/json"};

    final post = await http.put(
        "https://isysauthdb.herokuapp.com/api/authdb/cuenta/edit/estado",
        body: json.encode(params),
        headers: headers);

    print(post.statusCode.toString() + " - " + post.reasonPhrase);
    print(json.decode(post.body)['message']);
    pr.hide();
    if (post.statusCode == 200) {
      showBottomMessage("Cuenta eliminada satisfactoriamente");
      setState(() {
        globals.isLogged = false;
      });
      Navigator.pop(context);
      Navigator.pop(context);
    } else {
      showBottomMessage("Error al eliminar su cuenta");
      Navigator.pop(context);
    }
    return post;
  }

  bool _checkFields() {
    String pass = passController.text ?? "";
    String npass = npassController.text ?? "";

    if (pass.isNotEmpty && npass.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  Future<http.Response> _sendData() async {
    String pass = passController.text;
    String npass = npassController.text;

    print(pass + "`" + npass);

    if (_checkFields()) {
      setState(() {
        _isSending = true;
      });

      var params = {
        "correo": "" + globals.user['data']['correo'],
        "claveActual": "$pass",
        "claveNueva": "$npass"
      };

      Map<String, String> headers = {"Content-type": "application/json"};

      http.Response post = await http.put(
        "https://isysauthdb.herokuapp.com/api/authdb/cuenta/edit/clave",
        headers: headers,
        body: json.encode(params),
        //headers: paramDic
      );
      print("" + post.statusCode.toString() + " - " + post.reasonPhrase);
      if (post.statusCode == 200) {
        print("Success");
        var body = json.decode(post.body);
        setState(() {
          _isSending = false;
        });
        if (body['ok']) {
          showBottomMessage("Clave modificada exitosamente!");
          passController.text = "";
          npassController.text = "";
          Navigator.pop(context);
        } else {
          showBottomMessage("Clave no modificada");
          passController.text = "";
          npassController.text = "";
        }
      } else {
        print("ERROR");
      }

      return post;
    }
    return null;
  }

  Future<http.Response> _sendPt() async {
    String mail = mailController.text ?? "";
    int pt = int.parse(ptController.text) ?? 0;
    if (mail.isNotEmpty && pt > 0) {
      setState(() {
        _isSending = true;
      });

      var params = {
        "correo1": "" + globals.user['data']['correo'],
        "correo2": "$mail",
        "puntos": pt
      };

      Map<String, String> headers = {"Content-type": "application/json"};

      http.Response post = await http.post(
        "https://isysauthdb.herokuapp.com/api/authdb/cuenta/transfer/puntos",
        headers: headers,
        body: json.encode(params),
        //headers: paramDic
      );
      print("" + post.statusCode.toString() + " - " + post.reasonPhrase);
      if (post.statusCode == 200) {
        print("Success");
        var body = json.decode(post.body);
        setState(() {
          _isSending = false;
        });
        if (body['ok']) {
          showBottomMessage("Puntos Enviados Satisfactoriamente!");
          setState(() {
            globals.user['data']['puntos'] -= pt;
          });
          mailController.text = "";
          ptController.text = "";
          Navigator.pop(context);
        } else {
          showBottomMessage("Puntos No Enviados!");
          mailController.text = "";
          ptController.text = "";
        }
      } else {
        print("ERROR");
      }

      return post;
    } else {
      showBottomMessage("Revise los Campos");
      return null;
    }
  }

  _OpenChangePassDialog() {
    final cnpassController = TextEditingController();

    final passwordField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      color: Colors.white70,
      child: TextField(
        keyboardType: TextInputType.visiblePassword,
        controller: passController,
        textAlign: TextAlign.start,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Clave",
            prefixIcon: Icon(Icons.lock),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(15.0))),
      ),
    );

    final npasswordField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      color: Colors.white70,
      child: TextField(
        keyboardType: TextInputType.visiblePassword,
        controller: npassController,
        textAlign: TextAlign.start,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 15.0),
            hintText: "Nueva Clave",
            prefixIcon: Icon(Icons.lock),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(15.0))),
      ),
    );

    final cnpasswordField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
      color: Colors.white70,
      child: TextField(
        keyboardType: TextInputType.visiblePassword,
        controller: cnpassController,
        textAlign: TextAlign.start,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 15.0),
            hintText: "Confirmar Clave",
            prefixIcon: Icon(Icons.lock),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      ),
    );

    final confirmButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(20.0),
      color: Colors.blueAccent,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: _sendData,
        child: Text("Confirmar",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    showDialog(
        context: context,
        builder: (_) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0)),
              child: _isSending
                  ? new Center(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(globals.words[1][globals.lan]),
                          new CircularProgressIndicator()
                        ],
                      ),
                    )
                  : new ListView(
                      padding: EdgeInsets.all(16),
                      shrinkWrap: true,
                      children: <Widget>[
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                              "Cambiar Contraseña",
                              style: titleFont,
                            ),
                            new Padding(padding: EdgeInsets.only(bottom: 20)),
                            new Container(
                              height: 200,
                              child: new Column(
                                children: <Widget>[
                                  Flexible(
                                    fit: FlexFit.loose,
                                    child: passwordField,
                                  ),
                                  Flexible(
                                    fit: FlexFit.loose,
                                    child: npasswordField,
                                  ),
//                            Flexible(
//                              fit: FlexFit.loose,
//                              child: cnpasswordField,
//                            ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Flexible(
                                    fit: FlexFit.loose,
                                    child: confirmButton,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                        ),
                      ],
                    ));
        });
  }

  //Send Points
  _OpenPassPtDialog() {
    final emailField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      color: Colors.white70,
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        controller: mailController,
        textAlign: TextAlign.start,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Email Receptor",
            prefixIcon: Icon(Icons.alternate_email),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(15.0))),
      ),
    );

    final ptField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      color: Colors.white70,
      child: TextField(
        keyboardType:
            TextInputType.numberWithOptions(decimal: false, signed: false),
        controller: ptController,
        textAlign: TextAlign.start,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Puntos",
            prefixIcon: Icon(Icons.radio_button_unchecked),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(15.0))),
      ),
    );

    final confirmButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(20.0),
      color: Colors.blueAccent,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: _sendPt,
        child: Text("Confirmar",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    showDialog(
        context: context,
        builder: (_) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0)),
              child: _isSending
                  ? new Center(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(globals.words[1][globals.lan]),
                          new CircularProgressIndicator()
                        ],
                      ),
                    )
                  : new ListView(
                      padding: EdgeInsets.all(16),
                      shrinkWrap: true,
                      children: <Widget>[
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                              "Transferir Puntos",
                              style: titleFont,
                            ),
                            new Padding(padding: EdgeInsets.only(bottom: 20)),
                            new Container(
                              height: 200,
                              child: new Column(
                                children: <Widget>[
                                  Flexible(
                                    fit: FlexFit.loose,
                                    child: emailField,
                                  ),
                                  Flexible(
                                    fit: FlexFit.loose,
                                    child: ptField,
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Flexible(
                                    fit: FlexFit.loose,
                                    child: confirmButton,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                        ),
                      ],
                    ));
        });
  }
}
