import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:casa_museo_miss_trinie/classes/PostElements.dart';
import 'package:casa_museo_miss_trinie/classes/PostElement.dart';
import 'package:casa_museo_miss_trinie/classes/styles.dart';
import 'package:casa_museo_miss_trinie/classes/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';
import 'package:casa_museo_miss_trinie/routes/element.dart';

class Elements extends StatefulWidget {
  Elements({Key key, this.title, this.id}) : super(key: key);

  final String title;
  final String id;

  @override
  _ElementsState createState() => _ElementsState(id: id);
}

class _ElementsState extends State<Elements> {
  _ElementsState({this.id}) : super();

  final String id;
  Future<PostElements> postElements;

  Future<PostElements> _fetchDataElements() async {
    final response = await http.get(
        "http://ebostpay.herokuapp.com/listarPlanesPorSubcategoriaCopia/" +
            id +
            "/TRINIE@eBost");
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return PostElements.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    postElements = _fetchDataElements();
    print(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: new FutureBuilder<PostElements>(
          future: postElements,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return new ListView.builder(
                  padding: EdgeInsets.all(12),
                  itemBuilder: (BuildContext context, int index) {
                    return new Card(
                      elevation: 3,
                      child: ListTile(
                        trailing: Icon(Icons.arrow_forward),
                        title: Text(snapshot.data.elements[index]['descripcion']
                            [globals.lan]),
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Ele(
                                  title: widget.title,
                                  id: snapshot.data.elements[index]
                                      ['id_plan'])),
                        ),
                      ),
                    );
                  },
                  itemCount: snapshot.data.elements.length);
            } else {
              return new Center(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text(globals.words[1][globals.lan]),
                    new CircularProgressIndicator()
                  ],
                ),
              );
            }
          },
        ));
  }
}
