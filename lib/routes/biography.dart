import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:casa_museo_miss_trinie/classes/styles.dart';
import 'package:casa_museo_miss_trinie/classes/globals.dart' as globals;

class Biography extends StatefulWidget {
  Biography({Key key, this.title}) : super(key: key);

  final String title;

  @override
  BiographyState createState() => BiographyState();
}

class BiographyState extends State<Biography> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: widget.title,
      theme:
          ThemeData(primarySwatch: Colors.brown, accentColor: Colors.black87),
      debugShowCheckedModeBanner: false,
      debugShowMaterialGrid: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Text(globals.words[3][globals.lan], style: standardFontWh),
                Text(globals.words[4][globals.lan], style: standardFontWh),
              ],
            ),
            title: Row(
              children: [
                Flexible(
                  fit: FlexFit.loose,
                  child: IconButton(
                    onPressed: () => Navigator.of(context).pop(),
                    iconSize: 20,
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                      size: 20,
                    ),
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  child: CircleAvatar(
                    child: GestureDetector(
                      onTap: () => Navigator.of(context).pop(),
                    ),
                    backgroundImage: AssetImage("assets/images/foto2.jpg"),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 10),
                ),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      globals.texts[9][globals.lan],
                      style: subtitleFontWh,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.start,
                    ),
                    Text(
                      globals.texts[10][globals.lan],
                      style: standardFontWh,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.start,
                    ),
                  ],
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              new Column(
                children: <Widget>[
                  new Expanded(
                      flex: 1,
                      child: new SingleChildScrollView(
                        padding: EdgeInsets.all(16),
                        child: new Text(
                          "" + globals.articles[5][globals.lan],
                          style: standardFont,
                          textAlign: TextAlign.justify,
                        ),
                      ))
                ],
              ),
              new Column(
                children: <Widget>[
                  new Expanded(
                      child: new ListView.builder(
                          shrinkWrap: false,
                          itemCount:
                              (globals.gallery[1]['imagenes'] as List).length,
                          itemBuilder: (BuildContext context, int index) {
                            print('http://file-s.download' +
                                (globals.gallery[1]['imagenes'] as List)[index]
                                    ['imagen']);
                            return new Container(
                              child: FadeInImage.assetNetwork(
                                fit: BoxFit.cover,
                                placeholder: 'assets/images/spin_loading.gif',
                                image: 'http://file-s.download' +
                                    (globals.gallery[1]['imagenes']
                                        as List)[index]['imagen'],
                              ),
                            );
                          }))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
