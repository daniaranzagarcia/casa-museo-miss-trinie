import 'dart:convert';
import 'dart:io';

import 'package:casa_museo_miss_trinie/classes/Dialogs.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:casa_museo_miss_trinie/classes/styles.dart';
import 'package:http/http.dart' as http;
import 'package:casa_museo_miss_trinie/classes/globals.dart' as globals;

import '../main.dart';

class Registry extends StatefulWidget {
  Registry({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RegistryState createState() => _RegistryState();
}

class _RegistryState extends State<Registry> {
  String imgDefUri =
      "http://file-s.download/isysauthdb/webTransfer/imagenes/nobody.jpg";
  final mailController = TextEditingController();
  final passController = TextEditingController();
  final cpassController = TextEditingController();
  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  bool _isSending = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final nameField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
      color: Colors.white70,
      child: TextField(
        keyboardType: TextInputType.text,
        controller: nameController,
        textAlign: TextAlign.start,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Full name",
            prefixIcon: Icon(Icons.edit),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      ),
    );
    final emailField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
      color: Colors.white70,
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        controller: mailController,
        textAlign: TextAlign.start,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Email",
            prefixIcon: Icon(Icons.person),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      ),
    );
    final passwordField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
      color: Colors.white70,
      child: TextField(
        keyboardType: TextInputType.visiblePassword,
        controller: passController,
        textAlign: TextAlign.start,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Clave",
            prefixIcon: Icon(Icons.lock),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      ),
    );

    final cpasswordField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
      color: Colors.white70,
      child: TextField(
        keyboardType: TextInputType.visiblePassword,
        controller: cpassController,
        textAlign: TextAlign.start,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 15.0),
            hintText: "Confirmar",
            prefixIcon: Icon(Icons.lock),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      ),
    );

    final phoneField = Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
      color: Colors.white70,
      child: TextField(
        keyboardType: TextInputType.phone,
        controller: phoneController,
        textAlign: TextAlign.start,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Phone",
            prefixIcon: Icon(Icons.phone),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      ),
    );

    final registerButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.blueAccent,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: _sendData,
        child: Text("Register",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.title,
          textAlign: TextAlign.center,
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          new Center(
              child: Container(
            height: MediaQuery.of(context).size.height - 80,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_min.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: _isSending
                ? new Center(
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text(globals.words[1][globals.lan]),
                        new CircularProgressIndicator()
                      ],
                    ),
                  )
                : Padding(
                    padding: const EdgeInsets.all(36.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          decoration: ShapeDecoration(
                            shape: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(80),
                                borderSide:
                                    BorderSide(width: 3, color: Colors.white)),
                            color: Colors.white,
                          ),
                          child: CircleAvatar(
                            backgroundColor: Colors.white,
                            child: GestureDetector(),
                            radius: 80.0,
                            backgroundImage: FadeInImage.assetNetwork(
                                    fit: BoxFit.cover,
                                    placeholder:
                                        'assets/images/bar_loading.gif',
                                    image: imgDefUri)
                                .image,
                          ),
                        ),
                        SizedBox(height: 45.0),
                        Flexible(
                          child: nameField,
                          fit: FlexFit.loose,
                        ),
                        Flexible(
                          child: emailField,
                          fit: FlexFit.loose,
                        ),
                        Flexible(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                fit: FlexFit.loose,
                                child: passwordField,
                              ),
                              Flexible(
                                fit: FlexFit.loose,
                                child: cpasswordField,
                              ),
                            ],
                          ),
                          fit: FlexFit.loose,
                        ),
                        Flexible(
                          fit: FlexFit.loose,
                          child: phoneField,
                        ),
                        SizedBox(height: 10.0),
                        Flexible(
                          fit: FlexFit.loose,
                          child: registerButton,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                      ],
                    ),
                  ),
          ))
        ],
      ),
    );
  }

  bool _checkFields() {
    String name = nameController.text ?? "";
    String mail = mailController.text ?? "";
    String pass = passController.text ?? "";
    String cpass = cpassController.text ?? "";
    String phone = phoneController.text ?? "";

    if (name.isNotEmpty &&
        mail.isNotEmpty &&
        pass.isNotEmpty &&
        cpass.isNotEmpty &&
        phone.isNotEmpty) {
      if (pass == cpass) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  Future<http.Response> _sendData() async {
    String mail = mailController.text;
    String name = nameController.text;
    String pass = passController.text;
    String phone = phoneController.text;
    String img = imgDefUri;

    print(mail + name + pass + phone + img);

    if (_checkFields()) {
      setState(() {
        _isSending = true;
      });

      var params = {
        "correo": "$mail",
        "nombre_asociado": "$name",
        "telefono_movil": "$phone",
        "archivo_imagen_perfil":
            "https://ui-avatars.com/api/?name=$name&background=4c75a3&color=ECEFF1&size=256",
        "clave": "$pass",
        "autorizaciones": [
          {"proyecto": "Miss Trinie", "plataforma": "eBost cPanel"}
        ]
      };

      Map<String, String> headers = {"Content-type": "application/json"};

      http.Response post = await http.post(
        "https://isysauthdb.herokuapp.com/api/authdb/cuenta/add",
        headers: headers,
        body: json.encode(params),
        //headers: paramDic
      );
      print("" + post.statusCode.toString() + " - " + post.reasonPhrase);
      if (post.statusCode == 200) {
        print("Success");
        var body = json.decode(post.body);
        setState(() {
          _isSending = false;
        });
        if (body['ok']) {
          showBottomMessage("Cuenta creada exitosamente!");
          Navigator.pop(context);
        } else {
          showBottomMessage("Cuenta ya registrada");
          nameController.text = "";
          mailController.text = "";
          passController.text = "";
          cpassController.text = "";
          phoneController.text = "";
        }
      } else {
        print("ERROR");
      }

      return post;
    }
    return null;
  }
}
