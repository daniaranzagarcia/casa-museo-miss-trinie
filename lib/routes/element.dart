import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:casa_museo_miss_trinie/classes/PostElement.dart';
import 'package:casa_museo_miss_trinie/classes/styles.dart';
import 'package:casa_museo_miss_trinie/classes/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:casa_museo_miss_trinie/classes/site_dialog.dart';

class Ele extends StatefulWidget {
  Ele({Key key, this.title, this.id}) : super(key: key);

  final String title;
  final String id;

  @override
  _ElementState createState() => _ElementState(id: id);
}

class _ElementState extends State<Ele> {
  _ElementState({this.id}) : super();

  final String id;

  String url =
      'https://file-examples.com/wp-content/uploads/2017/11/file_example_MP3_700KB.mp3';

  Future<PostElement> _fetchDataElement() async {
    final response = await http.get(
        "http://ebostpay.herokuapp.com/listarInfoPlan/" +
            widget.id +
            "/TRINIE@eBost/tarifas/si");
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return PostElement.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<PostElement> postElement;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    postElement = _fetchDataElement();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: new FutureBuilder<PostElement>(
        future: postElement,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            int i = 0;
            switch (globals.lan) {
              case 'es':
                {
                  i = 0;
                  break;
                }
              case 'en':
                {
                  i = 1;
                  break;
                }
              case 'pt':
                {
                  i = 2;
                  break;
                }
              default:
                {
                  i = 0;
                }
            }
            url = 'https://file-s.download' +
                snapshot.data.element['data'][i]['valor'];
            print(url);

            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  child: FadeInImage.assetNetwork(
                      fit: BoxFit.cover,
                      placeholder: 'assets/images/spin_loading.gif',
                      image: 'http://file-s.download' +
                          snapshot.data.element['galeria']['imagenes'][0]
                              ['imagen']),
                ),
                new Expanded(
                    flex: 1,
                    child: new SingleChildScrollView(
                      padding: EdgeInsets.all(16),
                      child: Column(
                        children: <Widget>[
                          Text(
                              snapshot.data.element['descripcion'][globals.lan],
                              style: subtitleFont,
                              textAlign: TextAlign.center),
                          Divider(
                            thickness: 0.5,
                          ),
                          Text(
                              snapshot.data.element['metadatos'][0]['valor']
                                  [globals.lan],
                              style: standardFont,
                              textAlign: TextAlign.justify),
                          Padding(
                            padding: EdgeInsets.only(bottom: 80),
                          ),
                          FloatingActionButton(
                            onPressed: () => showDialog(
                                context: context,
                                builder: (_) => VideoPlayerScreen(
                                      url: url,
                                    )),
                            backgroundColor: Colors.redAccent,
                            child: Icon(Icons.volume_up),
                          ),
                        ],
                      ),
                    ))
              ],
            ));
          } else {
            return new Center(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(globals.words[1][globals.lan]),
                  new CircularProgressIndicator()
                ],
              ),
            );
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
/*

showDialog(
                context: context,
                builder: (_) => SiteDialogOverlay(
                  web: web,
                  mail: mail,
                  title: title,
                  lng: lng,
                  lat: lat,
                  id: id,
                  post: post,
                ),
              );


 */
