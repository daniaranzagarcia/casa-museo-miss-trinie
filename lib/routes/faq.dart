import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:casa_museo_miss_trinie/classes/globals.dart' as globals;
import 'package:casa_museo_miss_trinie/classes/styles.dart';

class Faq extends StatefulWidget {
  Faq({this.title}) : super();

  final String title;

  @override
  _FaqState createState() => _FaqState();
}

class _FaqState extends State<Faq> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              new Expanded(
                  flex: 1,
                  child: new SingleChildScrollView(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      children: <Widget>[
                        Text(globals.articles[3][globals.lan],
                            style: standardFont, textAlign: TextAlign.justify),
                        Divider(
                          thickness: 0.5,
                        ),
                        Text(globals.texts[20][globals.lan] + '\n',
                            style: subtitleFont, textAlign: TextAlign.justify),
                        Text(globals.articles[4][globals.lan],
                            style: standardFont, textAlign: TextAlign.justify),
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }
}
