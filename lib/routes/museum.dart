import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:casa_museo_miss_trinie/classes/styles.dart';
import 'package:casa_museo_miss_trinie/classes/PostSubCategories.dart';
import 'package:casa_museo_miss_trinie/routes/elements.dart';
import 'package:casa_museo_miss_trinie/classes/globals.dart' as globals;
import 'package:http/http.dart' as http;

Future<PostSubCategories> _fetchDataSubCategories() async {
  final response = await http
      .get("https://ebostpay.herokuapp.com/listarCategorias/TRINIE@eBost");
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    return PostSubCategories.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

class Museum extends StatefulWidget {
  Museum({Key key, this.title}) : super(key: key);

  final String title;

  @override
  MuseumState createState() => MuseumState();
}

class MuseumState extends State<Museum> {
  Future<PostSubCategories> postSubCategories;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    postSubCategories = _fetchDataSubCategories();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: widget.title,
      theme:
          ThemeData(primarySwatch: Colors.brown, accentColor: Colors.black54),
      debugShowCheckedModeBanner: false,
      debugShowMaterialGrid: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              labelStyle: ministandardFontWh,
              tabs: [
                Text(globals.words[5][globals.lan]),
                Text(globals.words[4][globals.lan]),
                Text(globals.words[6][globals.lan]),
              ],
            ),
            title: Row(
              children: [
                Flexible(
                  fit: FlexFit.loose,
                  child: IconButton(
                    onPressed: () => Navigator.of(context).pop(),
                    iconSize: 20,
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                      size: 20,
                    ),
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  child: CircleAvatar(
                    child: GestureDetector(
                      onTap: () => Navigator.of(context).pop(),
                    ),
                    backgroundImage: AssetImage("assets/images/foto2.jpg"),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 10),
                ),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      globals.texts[9][globals.lan],
                      style: subtitleFontWh,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.start,
                    ),
                    Text(
                      globals.texts[10][globals.lan],
                      style: standardFontWh,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.start,
                    ),
                  ],
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              new Column(
                children: <Widget>[
                  new Expanded(
                      flex: 1,
                      child: new SingleChildScrollView(
                        padding: EdgeInsets.all(16),
                        child: new Text(
                          "" + globals.articles[2][globals.lan],
                          style: standardFont,
                          textAlign: TextAlign.justify,
                        ),
                      ))
                ],
              ),
              new Column(
                children: <Widget>[
                  new Expanded(
                      child: new ListView.builder(
                          shrinkWrap: false,
                          itemCount:
                              (globals.gallery[0]['imagenes'] as List).length,
                          itemBuilder: (BuildContext context, int index) {
                            print('http://file-s.download' +
                                (globals.gallery[1]['imagenes'] as List)[index]
                                    ['imagen']);
                            return new Container(
                              child: FadeInImage.assetNetwork(
                                fit: BoxFit.cover,
                                placeholder: 'assets/images/spin_loading.gif',
                                image: 'http://file-s.download' +
                                    (globals.gallery[0]['imagenes']
                                        as List)[index]['imagen'],
                              ),
                            );
                          }))
                ],
              ),
              new FutureBuilder<PostSubCategories>(
                future: postSubCategories,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return new ListView.builder(
                        padding: EdgeInsets.all(12),
                        itemBuilder: (BuildContext context, int index) {
                          return new Card(
                            elevation: 3,
                            child: ListTile(
                              onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Elements(
                                          title: widget.title,
                                          id: snapshot.data.subCat[index]
                                              ['id_subcateg'],
                                        )),
                              ),
                              trailing: Icon(Icons.arrow_forward),
                              title: Text(snapshot.data.subCat[index]
                                  ['subcateg_' + globals.lan]),
                            ),
                          );
                        },
                        itemCount: snapshot.data.subCat.length);
                  } else {
                    return new Center(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(globals.words[1][globals.lan]),
                          new CircularProgressIndicator()
                        ],
                      ),
                    );
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
